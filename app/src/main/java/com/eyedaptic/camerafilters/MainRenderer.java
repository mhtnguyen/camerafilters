package com.eyedaptic.camerafilters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.opengl.GLES30;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Size;
import android.view.Surface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by minhhung on 12/2/2017.
 */

// Renderer
public class MainRenderer implements GLSurfaceView.Renderer, SurfaceTexture.OnFrameAvailableListener {

    private SurfaceTexture mSTexture;

    private boolean mGLInit = false;
    private boolean mUpdateST = false;

    private MainView mView;

    private CameraDevice mCameraDevice;
    private CameraCaptureSession mCaptureSession = null;
    private CaptureRequest.Builder mPreviewRequestBuilder;
    private String mCameraID;
    private Size mPreviewSize = new Size(1920, 1080);

    private HandlerThread mBackgroundThread;
    private Handler mBackgroundHandler;
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);
    private boolean need2Update = true;
    private final CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(CameraDevice cameraDevice) {
            mCameraOpenCloseLock.release();
            mCameraDevice = cameraDevice;
            createCameraPreviewSession();
        }

        @Override
        public void onDisconnected(CameraDevice cameraDevice) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(CameraDevice cameraDevice, int error) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

    };
    private ShaderSquare square;
    private Object locker = new Object();
    private FloatTextureData floatTextureData = null;

    MainRenderer(MainView view) {
        mView = view;
    }

    public void onResume() {
        startBackgroundThread();
    }

    public void onPause() {
        mGLInit = false;
        mUpdateST = false;
        closeCamera();
        stopBackgroundThread();
    }

    public ShaderSquare getSquare() {
        return square;
    }

    public boolean isNeed2Update() {
        return need2Update;
    }

    public void setNeed2Update(boolean need2Update) {
        this.need2Update = need2Update;
    }

    public FloatTextureData getFloatTextureData() {
        return floatTextureData;
    }

    public void setFloatTextureData(FloatTextureData floatTextureData) {
        this.floatTextureData = floatTextureData;
    }

    public void onSurfaceCreated(GL10 unused, javax.microedition.khronos.egl.EGLConfig config) {
        //String extensions = GLES20.glGetString(GLES20.GL_EXTENSIONS);
        //Log.i("mr", "Gl extensions: " + extensions);
        //Assert.assertTrue(extensions.contains("OES_EGL_image_external"));
        //If authorization not granted for camera
        if (ContextCompat.checkSelfPermission(mView.getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            //ask for authorisation
            ActivityCompat.requestPermissions((Activity) mView.getContext(), new String[]{Manifest.permission.CAMERA}, 50);
        else {

            Point ss = new Point();
            mView.getDisplay().getRealSize(ss);

            //Create a rectangle to draw camera image and all other object using shading method
            //pass in width and height of screen

            //for distortion data
            floatTextureData = new FloatTextureData(ss.x,ss.y);

            //keep at lower for older phones for now
            square = new ShaderSquare(ss.x,ss.y);
            square.setDistortionTexture(floatTextureData.getTexture());
            square.setupBuffer();
            //square = new ShaderSquare(1920,1080);
            mSTexture = new SurfaceTexture(square.hTex[0]);
            mSTexture.setOnFrameAvailableListener(this);

            GLES30.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

            cacPreviewSize(ss.x, ss.y);
            openCamera();

            mGLInit = true;
        }
    }

    public void onDrawFrame(GL10 unused) {
        if (!mGLInit) return;
        GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT);

        synchronized (locker) {
            if (mUpdateST) {
                mSTexture.updateTexImage();
                mUpdateST = false;
            }
        }
        //controls version 2
        if(need2Update){
            floatTextureData.updateTable(square.getAngle());
            need2Update = false;
        }
        if (square != null) {
            square.draw();
        }

    }

    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES30.glViewport(0, 0, width, height);
        GLES30.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    }

    public  void onFrameAvailable(SurfaceTexture st) {
        synchronized (locker) {
            mUpdateST = true;
            mView.requestRender();
        }
    }

    void cacPreviewSize(final int width, final int height) {
        CameraManager manager = (CameraManager) mView.getContext().getSystemService(Context.CAMERA_SERVICE);
        try {
            for (String cameraID : manager.getCameraIdList()) {
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraID);
                if (characteristics.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT)
                    continue;

                mCameraID = cameraID;
                List<Size> bigEnough = new ArrayList<Size>();
                StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                Size[] choices = map.getOutputSizes(SurfaceTexture.class);
                for (Size psize : choices) {
                    if (psize.getHeight() == psize.getWidth() * height/width
                            && psize.getWidth()>= width && psize.getHeight()>=height) {
                        bigEnough.add(psize);
                    }
                }
                if(bigEnough.size() > 0){
                    mPreviewSize = Collections.min(bigEnough, new CompareSizeByArea());
                }
                else{
                    mPreviewSize = choices[0];
                }

                break;
            }
        } catch (CameraAccessException e) {
            Log.e("mr", "cacPreviewSize - Camera Access Exception");
        } catch (IllegalArgumentException e) {
            Log.e("mr", "cacPreviewSize - Illegal Argument Exception");
        } catch (SecurityException e) {
            Log.e("mr", "cacPreviewSize - Security Exception");
        }
    }

    private static class CompareSizeByArea implements Comparator<Size> {

        @Override
        public int compare(Size o1, Size o2) {
            return Long.signum((long) o1.getWidth() * o1.getHeight());
        }
    }

    void openCamera() {
        CameraManager manager = (CameraManager) mView.getContext().getSystemService(Context.CAMERA_SERVICE);
        try {
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(mCameraID);
            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            manager.openCamera(mCameraID, mStateCallback, mBackgroundHandler);
        } catch (CameraAccessException e) {
            Log.e("mr", "OpenCamera - Camera Access Exception");
        } catch (IllegalArgumentException e) {
            Log.e("mr", "OpenCamera - Illegal Argument Exception");
        } catch (SecurityException e) {
            Log.e("mr", "OpenCamera - Security Exception");
        } catch (InterruptedException e) {
            Log.e("mr", "OpenCamera - Interrupted Exception");
        }
    }

    private void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();
            if (null != mCaptureSession) {
                mCaptureSession.close();
                mCaptureSession = null;
            }
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.", e);
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    private void createCameraPreviewSession() {
        try {
            mSTexture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());

            Surface surface = new Surface(mSTexture);

            mPreviewRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mPreviewRequestBuilder.addTarget(surface);

            mCameraDevice.createCaptureSession(Arrays.asList(surface),
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(CameraCaptureSession cameraCaptureSession) {
                            if (null == mCameraDevice)
                                return;

                            mCaptureSession = cameraCaptureSession;
                            try {
                                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);

                                mCaptureSession.setRepeatingRequest(mPreviewRequestBuilder.build(), null, mBackgroundHandler);
                            } catch (CameraAccessException e) {
                                Log.e("mr", "createCaptureSession");
                            }
                        }

                        @Override
                        public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
                        }
                    }, null
            );
        } catch (CameraAccessException e) {
            Log.e("mr", "createCameraPreviewSession");
        }
    }

    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            Log.e("mr", "stopBackgroundThread");
        }
    }
}

