package com.eyedaptic.camerafilters.activity;

import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.SeekBar;

import com.eyedaptic.camerafilters.R;
import com.eyedaptic.camerafilters.utils.CameraHelper;
import com.eyedaptic.camerafilters.utils.GPUImageFilterTools;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;

import static com.eyedaptic.camerafilters.utils.GPUImageFilterTools.*;

public class CameraActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener, View.OnClickListener {
    //    private MainView mView;
//    private PowerManager.WakeLock mWL;
    float c = 0.5f;
    float xAxis, yAxis;
    boolean alpha = false;
    boolean version1 = true;
    float angle = 0;
    private GPUImage mGPUImage;
    private CameraHelper mCameraHelper;
    private CameraLoader mCamera;
    private GPUImageFilter mFilter;
    private FilterAdjuster mFilterAdjuster;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // full screen & full brightness
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        mWL = ((PowerManager)getSystemService ( Context.POWER_SERVICE )).newWakeLock(PowerManager.FULL_WAKE_LOCK, "WakeLock");
//        mWL.acquire();
//        mView = new MainView(this);
//        setContentView ( mView );
        setContentView(R.layout.activity_main);

        ((SeekBar) findViewById(R.id.seekBar)).setOnSeekBarChangeListener(this);
        findViewById(R.id.button_choose_filter).setOnClickListener(this);
        mGPUImage = new GPUImage(this);
        mGPUImage.setGLSurfaceView((GLSurfaceView) findViewById(R.id.surfaceView));

        mCameraHelper = new CameraHelper(this);
        mCamera = new CameraLoader();

        View cameraSwitchView = findViewById(R.id.img_switch_camera);
        cameraSwitchView.setOnClickListener(this);
        if (!mCameraHelper.hasFrontCamera() || !mCameraHelper.hasBackCamera()) {
            cameraSwitchView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (grantResults.length == 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onPause() {
        if (mCamera != null) {
            mCamera.onPause();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mCamera != null) {
            mCamera.onResume();
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent KEvent) {
        int keyaction = KEvent.getAction();

        if (keyaction == KeyEvent.ACTION_DOWN) {
            int keycode = KEvent.getKeyCode();
            switch (keycode) {
                case KeyEvent.KEYCODE_A:
                    if (c < 1.0) {
                        c += .05;
                    }
                    break;
                case KeyEvent.KEYCODE_S:
                    if (c > 0) {
                        c -= .05;
                    }
                    break;
                case KeyEvent.KEYCODE_Q:
                    c = (float) 0.5;
                    break;
                case KeyEvent.KEYCODE_DPAD_LEFT:
                    if (xAxis > -1) {
                        xAxis -= (float) 0.025;
                    }
                    break;
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    if (xAxis < 1.0) {
                        xAxis += (float) 0.025;
                    }
                    break;
                case KeyEvent.KEYCODE_DPAD_UP:
                    if (yAxis < 0.5) {
                        yAxis += (float) 0.025;
                    }
                    break;
                case KeyEvent.KEYCODE_DPAD_DOWN:
                    if (yAxis > -.5) {
                        yAxis -= (float) 0.025;
                    }
                    break;
                case KeyEvent.KEYCODE_SPACE:
                    xAxis = 0;
                    yAxis = 0;
                    break;
                case KeyEvent.KEYCODE_SHIFT_RIGHT:
                    if (alpha) {
                        alpha = false;
                    } else {
                        alpha = true;
                    }
                    break;
                    /* We will leave center and radius alone, and provide a keyboard control that adjusts angle over the range of 0.0 (the starting value)
                     to 1.0 with a step size no larger than 0.05 (actually 0.025 is probably about right). A second keyboard key must be assigned to
                     cycle through three types of Layer 3 processing: the version 1 shader (below), the version 2 shader (further below),
                     or a disabled state where there is no Layer 3 processing at all.
                     */
                case KeyEvent.KEYCODE_V:
                    if (version1) {
                        version1 = false;
                    } else {
                        version1 = true;
                    }
                    break;
                //had to change keys  because my keyboard broke
                case KeyEvent.KEYCODE_K:
                    if (angle > 0.0f) {
                        angle -= (float) 0.025;
                    }
                    break;
                case KeyEvent.KEYCODE_L:
                    if (angle < 1.0f) {
                        angle += (float) 0.025;
                    }
                    break;
                default:
                    c = (float) 0.5;
                    xAxis = 0;
                    yAxis = 0;
                    alpha = false;
                    version1 = true;
                    angle = 0;
                    break;
            }
//            if(mView!=null && mView.mRenderer!=null ){
//                ShaderSquare square = mView.mRenderer.getSquare();
//                if(square!=null){
//                    if(version1) {
//                        square.setVersion1(0.0f);
//                        mView.mRenderer.setNeed2Update(false);
//                    }else{
//                        square.setVersion1(1.0f);
//                        mView.mRenderer.setNeed2Update(true);
//                    }
//                    square.setC((float) (Math.round(c*100.0)/100.0));
//                    square.setShiftX((float) (Math.round(xAxis*100.0)/100.0));
//                    square.setShiftY((float) (Math.round(yAxis*100.0)/100.0));
//                    square.setAngle((float) (Math.round(angle*100.0)/100.0));
//                    float newC = square.getC();
//                    float newX = square.getShiftX();
//                    float newY = square.getShiftY();
//                    //set new center here
//                    if(alpha){
//                        square.setAlpha(0f);
//                    }else{
//                        square.setAlpha(0.5f);
//                    }
//                    setTitle("KEY="+ KeyEvent.keyCodeToString(keycode)
//                            +" c: "+newC +" x: "+newX + " y: "+newY + " alpha: "+(alpha ? "100%" : "50%")
//                            +" angle: "+angle+" version: "+ (square.getVersion1()+1.0f));
//                    square.draw();
//                }
//            }
        }
        return super.dispatchKeyEvent(KEvent);
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.button_choose_filter:
                //create dialog with filters.
                GPUImageFilterTools.showDialog(this, new OnGpuImageFilterChosenListener() {

                    @Override
                    public void onGpuImageFilterChosenListener(final GPUImageFilter filter) {
                        switchFilterTo(filter);
                    }
                });
                break;

            case R.id.img_switch_camera:
                mCamera.switchCamera();
                break;
        }
    }

    private void switchFilterTo(final GPUImageFilter filter) {
        if (mFilter == null
                || (filter != null && !mFilter.getClass().equals(filter.getClass()))) {
            mFilter = filter;
            mGPUImage.setFilter(mFilter);
            mFilterAdjuster = new FilterAdjuster(mFilter);
        }
    }

    @Override
    public void onProgressChanged(final SeekBar seekBar, final int progress,
                                  final boolean fromUser) {
        if (mFilterAdjuster != null) {
            mFilterAdjuster.adjust(progress);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    private class CameraLoader {

        private int mCurrentCameraId = 0;
        private Camera mCameraInstance;

        public void onResume() {
            setUpCamera(mCurrentCameraId);
        }

        public void onPause() {
            releaseCamera();
        }

        public void switchCamera() {
            releaseCamera();
            mCurrentCameraId = (mCurrentCameraId + 1) % mCameraHelper.getNumberOfCameras();
            setUpCamera(mCurrentCameraId);
        }

        private void setUpCamera(final int id) {
            mCameraInstance = getCameraInstance(id);
            Camera.Parameters parameters = mCameraInstance.getParameters();
            // TODO adjust by getting supportedPreviewSizes and then choosing
            // the best one for screen size (best fill screen)
            if (parameters.getSupportedFocusModes().contains(
                    Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            }
            mCameraInstance.setParameters(parameters);

            int orientation = mCameraHelper.getCameraDisplayOrientation(
                    CameraActivity.this, mCurrentCameraId);
            CameraHelper.CameraInfo2 cameraInfo = new CameraHelper.CameraInfo2();
            mCameraHelper.getCameraInfo(mCurrentCameraId, cameraInfo);
            boolean flipHorizontal = cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT;
            mGPUImage.setUpCamera(mCameraInstance, orientation, flipHorizontal, false);
        }

        /**
         * A safe way to get an instance of the Camera object.
         */
        private Camera getCameraInstance(final int id) {
            Camera c = null;
            try {
                c = mCameraHelper.openCamera(id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return c;
        }

        private void releaseCamera() {
            mCameraInstance.setPreviewCallback(null);
            mCameraInstance.release();
            mCameraInstance = null;
        }
    }
}



