package com.eyedaptic.camerafilters;

import android.opengl.GLES11Ext;
import android.opengl.GLES30;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * A two-dimensional square for use as a drawn object in OpenGL ES 2.0.
 */
public class ShaderSquare {
    // number of coordinates per vertex in this array
    static final int COORDS_PER_VERTEX = 3;
    static final int COORDS_PER_UV = 2;
    static float squareCoords[] = {
            -1f, 1f, 0.0f,   // top left
            -1f, -1f, 0.0f,   // bottom left
            1f, -1f, 0.0f,   // bottom right
            1f, 1f, 0.0f}; // top right

    //Draw crosshair using shading
    private final String vertexShaderCode =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader
            "attribute vec3 aPosition;\n" +
                    "attribute vec2 aTexCoordinate; // Per-vertex texture coordinate information we will pass in.\n" +
                    "varying vec2 v_TexCoordinate; // Interpolated texture coordinate per fragment.\n" +

                    "void main() {\n" +
                    "  gl_Position = vec4(aPosition,1.0);// gl_position is builtin output required for vertex shader\n" +
                    "v_TexCoordinate = aTexCoordinate;// Pass through the texture coordinate.\n" +
                    "}";


    private final String fragmentShaderCodeDistortion2 =
            "#extension GL_OES_EGL_image_external : require\n" +
                    "precision mediump float;\n" +
                    "uniform samplerExternalOES uTexture;\n" +
                    "uniform sampler2D uTexture1;\n" +
                    "uniform float uC;\n" +
                    "uniform float uAlpha;\n" +
                    "uniform vec2 uWindowSize;\n"+
                    "//current center of crosshair x,y\n"+
                    "uniform vec2 uCrossHairPosition;\n"+
                    "uniform float uVersion;\n"+
                    "uniform float uAngle;//will be uniform\n"+
                    "uniform vec2 uCenter = vec2(0.4, 0.4);\n"+
                    "float uRadius = 0.55 ;//will be uniform\n"+
                    "varying vec2 v_TexCoordinate; // Interpolated texture coordinate per fragment.\n" +
                    "//This function run for each pixel of window, using pixel coordinate and mathematical function of shapes, we can draw shapes\n"+
                    "\n"+
                    "vec4 drawCrossHair(vec2 position,vec2 size){\n" +
                    "vec2 xy = gl_FragCoord.xy;//Coordinate of current pixel\n" +
                    "vec4 color = vec4(0.0,1.0,0.0,0.0);//Color output. Default alpha =0, no draw.\n" +
                    "int active = 0;\n" +
                    "//Horizontal line in between border of size x and size y\n"+
                    "if(xy.y > position.y - size.y && xy.y < position.y + size.y &&  xy.x > position.x - size.x && xy.x < position.x + size.x ){\n" +
                    "active = 1;\n" +
                    "}\n" +
                    "//Vertical line\n"+
                    "else if(xy.y > position.y - size.x && xy.y < position.y + size.x && xy.x > position.x - size.y && xy.x < position.x + size.y ){\n" +
                    "active = 2;\n" +
                    "}\n" +
                    "if(active > 0){\n" +
                    "color.a = uAlpha;\n" +
                    "}\n" +
                    "return color;}\n" +
                    "vec2 undistort2(vec2 tex_coord0){ \n"+
                    " vec2 textureCoordinateToUse = texture2D(uTexture1, tex_coord0).rg ;\n"+
                    "return textureCoordinateToUse;\n"+
                    "}\n"+
                       /*Version 1 of this distortion effect is implemented entirely by a provided fragment shader,
                    and performs significant computations on each pixel.
                     The amount of distortion will be controlled by a parameter that can be adjusted using the keyboard in real-time. (angle)
                      Layer 1 fragment shader works: it is invoked for each desired output pixel, with the pixel coordinates in uniform tex_coord0.
                      It then samples the corresponding pixel from the input image (uniform texture0) before making an adjustment
                      to that soruce pixel's color to produce the output pixel.
                      We will leave center and radius alone, and provide a keyboard control that adjusts angle over the range
                      of 0.0 (the starting value) to 1.0 with a step size no larger than 0.05 (actually 0.025 is probably about right).
                     */
                    "vec2 undistort1(vec2 tex_coord0,vec2 center, float radius, float angle){\n"+
                    "vec2 textureCoordinateToUse = tex_coord0;\n"+
                    "float dist = distance(center, tex_coord0);\n"+
                    "if (dist < radius) {\n"+
                    "textureCoordinateToUse -= center;\n"+
                    "float percent = (radius - dist) / radius;\n"+
                    "float theta = percent * percent * angle * 8.0;\n"+
                    "float s = sin(theta);\n"+
                    "float c = cos(theta);\n"+
                    "textureCoordinateToUse = vec2(dot(textureCoordinateToUse, vec2(c, -s)), dot(textureCoordinateToUse, vec2(s, c)));\n"+
                    "textureCoordinateToUse += center;\n"+
                    "}\n"+
                    "return textureCoordinateToUse;\n"+
                    "}\n"+
                    "void main() {\n" +
                    "vec2 texCoordinate;\n"+
                    "if(uVersion==0.0f){\n"+
                    "texCoordinate = undistort1(v_TexCoordinate,uCenter,uRadius,uAngle);\n"+
                    "}\n"+
                    "else{\n"+
                    "texCoordinate = undistort2(v_TexCoordinate);\n"+
                    "}\n"+
                    "  vec4 color = texture2D(uTexture, texCoordinate);\n" +
                    " color = vec4((color.rgb-vec3(0.5))*uC+vec3(0.5), color.w) ;\n" +
                    " vec4 crosshair = drawCrossHair(uCrossHairPosition,vec2(0.1*min(uWindowSize.x,uWindowSize.y),10));\n"+
                    "  gl_FragColor = mix(color,crosshair,crosshair.a);\n" +
                    "}";
    private FloatBuffer vertexBuffer;
    private FloatBuffer uvBuffer;
    private ShortBuffer indexBuffer;
    private int mProgram;
    private short indexData[] = {0, 1, 2, 0, 2, 3}; // order to draw vertices
    public int[] hTex;
    //4 corners of image rectangle
    float uvs[] = {0, 0, // bottom left corner
            0, 1, // top left corner
            1, 1, // top right corner
            1, 0}; // bottom right corner

    private int mPositionHandle;
    private int mTextureUniformHandle;
    private int mTexture1UniformHandle;
    private int mTextureCoordinateHandle;
    static float version1 = 0;
    static float c = 0.5f;
    static float angle = 0;
    static float alpha = 0.5f;
    static float shiftX = 0,shiftY= 0;
    private int uAngleLocation;
    //private int uCenterLocation;
    private int uVersionLocation;
    private int uCLocation;
    private int uAlphaLocation;
    private int mWindowSizeUniformHandle;
    private int mCrossHairUniformHandle;
    private int w;
    private int h;
    private  int distortionTexture;

    public void setDistortionTexture(int textureID){
        distortionTexture = textureID;
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }
    /**
     * Sets up the drawing object data for use in an OpenGL ES context.
     */
    public ShaderSquare(int w,int h) {
        setW(w);
        setH(h);
        setupBuffer();
    }

    public void setupBuffer(){

        // initialize vertex byte buffer for shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect(
                // (# of coordinate values * 4 bytes per float)
                squareCoords.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(squareCoords);
        vertexBuffer.position(0);

        //uv
        ByteBuffer bbuv = ByteBuffer.allocateDirect(
                // (# of coordinate values * 4 bytes per float)
                uvs.length * 4);
        bbuv.order(ByteOrder.nativeOrder());
        uvBuffer = bbuv.asFloatBuffer();
        uvBuffer.put(uvs);
        uvBuffer.position(0);

        // initialize byte buffer for index
        ByteBuffer dlb = ByteBuffer.allocateDirect(
                // (# of coordinate values * 2 bytes per short)
                indexData.length * 2);
        dlb.order(ByteOrder.nativeOrder());
        indexBuffer = dlb.asShortBuffer();
        indexBuffer.put(indexData);
        indexBuffer.position(0);

        //texture
        //generate id
        hTex = new int[1];
        GLES30.glGenTextures(1, hTex, 0);
        GLES30.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, hTex[0]);

        // Set filtering
        GLES30.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES30.GL_TEXTURE_MIN_FILTER, GLES30.GL_LINEAR);
        GLES30.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES30.GL_TEXTURE_MAG_FILTER, GLES30.GL_LINEAR);

        // prepare shaders and OpenGL program
        mProgram = loadShader(vertexShaderCode, fragmentShaderCodeDistortion2);

        // Add program to OpenGL environment
        GLES30.glUseProgram(mProgram);
        // get handle to vertex shader's vPosition member
        mPositionHandle = GLES30.glGetAttribLocation(mProgram, "aPosition");
        mTextureCoordinateHandle = GLES30.glGetAttribLocation(mProgram, "aTexCoordinate");
        // Enable a handle to the triangle vertices
        GLES30.glEnableVertexAttribArray(mPositionHandle);
        // Prepare the triangle coordinate data
        GLES30.glVertexAttribPointer(
                mPositionHandle, COORDS_PER_VERTEX,
                GLES30.GL_FLOAT, false,
                0, vertexBuffer);
        GLES30.glEnableVertexAttribArray(mTextureCoordinateHandle);
        GLES30.glVertexAttribPointer(
                mTextureCoordinateHandle, COORDS_PER_UV,
                GLES30.GL_FLOAT, false,
                0, uvBuffer);

        //Map with shader(will move to init)
        mTextureUniformHandle = GLES30.glGetUniformLocation(mProgram, "uTexture");
        mTexture1UniformHandle = GLES30.glGetUniformLocation(mProgram, "uTexture1");
        // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
        GLES30.glUniform1i(mTextureUniformHandle, 0);
        GLES30.glUniform1i(mTexture1UniformHandle, 1);
        // Set the active texture unit to texture unit 0.
        GLES30.glActiveTexture(GLES30.GL_TEXTURE0);
        GLES30.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, hTex[0]);
        GLES30.glActiveTexture(GLES30.GL_TEXTURE1);
        // The amount of distortion will be controlled by a parameter that can be adjusted using the keyboard in real-time.
        GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, distortionTexture);


        //Get uniform location first then pass in value
        mWindowSizeUniformHandle = GLES30.glGetUniformLocation(mProgram, "uWindowSize");
        //Pass window size to fragment shader for calculation
        GLES30.glUniform2f(mWindowSizeUniformHandle,w,h);

        mCrossHairUniformHandle = GLES30.glGetUniformLocation(mProgram, "uCrossHairPosition");
        uVersionLocation = GLES30.glGetUniformLocation(mProgram, "uVersion");
        uAngleLocation = GLES30.glGetUniformLocation(mProgram, "uAngle");
        //uCenterLocation = GLES30.glGetUniformLocation(mProgram, "uCenter");
        uCLocation = GLES30.glGetUniformLocation(mProgram, "uC");
        uAlphaLocation = GLES30.glGetUniformLocation(mProgram, "uAlpha");
    }

    public synchronized float getC() {
        return c;
    }

    public static float getVersion1() {
        return version1;
    }

    public synchronized void setVersion1(float version1) {
        this.version1 = version1;
    }

    public synchronized void setC(float c) {
        this.c = c;
    }

    public synchronized float getShiftX() {
        return shiftX;
    }

    public synchronized void setShiftX(float shiftX) {
        this.shiftX = shiftX;
    }

    public synchronized float getShiftY() {
        return shiftY;
    }

    public synchronized void setShiftY(float shiftY) {
        this.shiftY = shiftY;
    }

    public synchronized float getAngle() {
        return angle;
    }

    public synchronized void setAngle(float angle) {
        this.angle = angle;
    }

    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

    private static int loadShader(String vss, String fss) {
        int vshader = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
        GLES30.glShaderSource(vshader, vss);
        GLES30.glCompileShader(vshader);
        int[] compiled = new int[1];
        GLES30.glGetShaderiv(vshader, GLES30.GL_COMPILE_STATUS, compiled, 0);
        if (compiled[0] == 0) {
            Log.e("Shader", "Could not compile vshader");
            Log.v("Shader", "Could not compile vshader:" + GLES30.glGetShaderInfoLog(vshader));
            GLES30.glDeleteShader(vshader);
            vshader = 0;
        }

        int fshader = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);
        GLES30.glShaderSource(fshader, fss);
        GLES30.glCompileShader(fshader);
        GLES30.glGetShaderiv(fshader, GLES30.GL_COMPILE_STATUS, compiled, 0);
        if (compiled[0] == 0) {
            Log.e("Shader", "Could not compile fshader");
            Log.v("Shader", "Could not compile fshader:" + GLES30.glGetShaderInfoLog(fshader));
            GLES30.glDeleteShader(fshader);
            fshader = 0;
        }

        int program = GLES30.glCreateProgram();
        GLES30.glAttachShader(program, vshader);
        GLES30.glAttachShader(program, fshader);
        GLES30.glLinkProgram(program);

        return program;
    }

    /**
     * Encapsulates the OpenGL ES instructions for drawing this shape.
     */
    public void draw() {
        GLES30.glUseProgram(mProgram);
        //angle
        GLES30.glUniform1f(uAngleLocation, getAngle());
        //version
        GLES30.glUniform1f(uVersionLocation, getVersion1());
        //C
        GLES30.glUniform1f(uCLocation, getC());
        //alpha
        GLES30.glUniform1f(uAlphaLocation, getAlpha());
        //cross hair
        int smallDimension = Math.min(getW(),getH());
        //Pass arrow key values to fragment shader for calculation
        //0 + (720/2) position x, 0+(1080/2) position y
        //720*.025 + (720/2), 720*0.25 +(1080/2)
        GLES30.glUniform2f(mCrossHairUniformHandle,smallDimension*shiftX + (getW()/2), smallDimension*shiftY + (getH()/2));
        //center
        //GLES30.glUniform2f(uCenterLocation,shiftX + 0.5f,0.5f-shiftY);
        // Draw the square
        GLES30.glDrawElements(
                GLES30.GL_TRIANGLES, indexData.length,
                GLES30.GL_UNSIGNED_SHORT, indexBuffer);
    }
}
