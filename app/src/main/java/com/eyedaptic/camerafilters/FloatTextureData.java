package com.eyedaptic.camerafilters;

import android.graphics.PointF;
import android.opengl.GLES30;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * A two-dimensional square for use as a drawn object in OpenGL ES 2.0.
 */
public class FloatTextureData {

    private int w;
    private int h;

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public int[] hTex = {0};
    float table[];
    private FloatBuffer tableBuffer;

    public int getTexture() {
        return hTex[0];
    }

    // vertically centered and approximately 55% of the screen width from the left edge, 45% from the right).
    PointF center = new PointF(0.4f, 0.4f);
    float radius = 0.55f;

    /**
     * Sets up the drawing object data for use in an OpenGL ES context.
     */
    public FloatTextureData(int w, int h) {
        setW(w);
        setH(h);
        setupFloatTextureData();
    }

    private void setupFloatTextureData() {
        // The texture we're going to render to
        hTex = new int[1];
        GLES30.glGenTextures(1, hTex, 0);

        // "Bind" the newly created texture : all future texture functions will modify this texture
        GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, hTex[0]);

        // Give an empty image to OpenGL ( the last "0" )
        ByteBuffer bbuv = ByteBuffer.allocateDirect(
                // (# of pixels * 4 bytes per float * # of PointF dimension
                w * h * 4 * 2);
        bbuv.order(ByteOrder.nativeOrder());
        tableBuffer = bbuv.asFloatBuffer();
        tableBuffer.position(0);
        // Poor filtering. Needed !
        GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MAG_FILTER, GLES30.GL_NEAREST);
        GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MIN_FILTER, GLES30.GL_NEAREST);


        //Generate first time
        table = new float[w * h * 2];
    }

    private float distance(PointF a, PointF b) {
        return PointF.length(a.x - b.x, a.y - b.y);
    }

    private float dot(PointF a, PointF b) {
        return a.x * b.x + a.y * b.y;
    }

    /*
        Version 2 is a simplified fragment shader (also provided) that performs almost no computations; instead,
        the application must maintain a lookup table that defines the mapping. This lookup table will have the same dimensions
        as the input image, and only changes when the control parameter changes. The implementation must arrange for this lookup table
        to be computed efficiently (i.e. only when necessary), converted into an efficient texture suitable for use by an OpenGL shader
        (e.g. with a suitable format, not necessarily RGBA since it only has two components), and used efficiently
        (i.e. no unnecessary multiple transfers to OpenGL).
     */

    /**
     * Update table data
     *
     * @param angle
     */
    public void updateTable(float angle) {
        Log.e("camera", w + " " + h);
        for (int j = 0; j < h; j++)
            for (int i = 0; i < w; i++) {
                int index = j * w + i;
                /**
                 * PointF holds two float coordinates
                 */
                PointF uv = new PointF(i * 1.0f / w, j * 1.0f / h);
                PointF tmp = undistort1(uv, center, radius, angle);
                table[index * 2] = tmp.x;
                table[index * 2 + 1] = tmp.y;
            }

        tableBuffer.clear();
        tableBuffer.put(table);
        tableBuffer.position(0);
        GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, hTex[0]);
        GLES30.glTexImage2D(GLES30.GL_TEXTURE_2D, 0, GLES30.GL_RG32F, w, h, 0, GLES30.GL_RG, GLES30.GL_FLOAT, tableBuffer);
    }

    public PointF undistort1(PointF tex_coord0, PointF center, float radius, float angle) {

        PointF textureCoordinateToUse = tex_coord0;
        float dist = distance(center, tex_coord0);
        if (dist < radius) {
            textureCoordinateToUse.x -= center.x;
            textureCoordinateToUse.y -= center.y;
            float percent = (radius - dist) / radius;
            float theta = percent * percent * angle * 8.0f;
            float s = (float) Math.sin(theta);
            float c = (float) Math.cos(theta);
            textureCoordinateToUse = new PointF(dot(textureCoordinateToUse, new PointF(c, -s)), dot(textureCoordinateToUse, new PointF(s, c)));
            textureCoordinateToUse.x += center.x;
            textureCoordinateToUse.y += center.y;
        }
        return textureCoordinateToUse;
    }
}
